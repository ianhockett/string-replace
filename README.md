# Examples of string replacement

Below are a couple of solutions to a particular problem, where an email address needs ot be replaced in all postfix configuration files. The solution is given using sed/grep, and powershell.

##### sed/grep
```bash
sudo sed -i 's/user@olddomain.local/user@newdomain.local/g' $(grep -rl user@olddomain.local /etc/postfix)
```

##### powershell
```powershell
Get-ChildItem -Path "/etc/postfix" | ForEach-Object {
    $Content = Get-Content $_.FullName
    if ($Content -match "user@olddomain.local") {
        $Content.Replace("user@olddomain.local","user@newdomain.local") | Set-Content -Path $_.FullName
    }
}
```